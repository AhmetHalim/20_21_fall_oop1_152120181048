#include <iostream>
#include <stdlib.h>
#include <fstream>

using namespace std;

int SumOperator(int *A, int);
int productOperator(int *A, int);
float averageOperator(int *A, int);
int smallestOperator(int *A, int);

int main()
{
	//! Opening the input.txt file that needs to be read.
	fstream textFile;
	textFile.open("input.txt", ios::in);
	int number, *variables;
	int counter = 0;

	//! Error checking if a different file name is read.
	if (!textFile)
	{
		cout << "ERROR! Filename is not 'input.txt'." << endl;
		system("pause");
		return 0;
	}

	//! Reading the first variable from input.txt file.
	textFile >> number; 

	//! Checking the error that will occur if negative and zero are entered for the first variable entered.
	if (number <= 0)
	{
		cout << "ERROR! Number cannot equal zero or negative." << endl;
		system("pause");
		return 0;
	}

	variables = new int[number];

	//! Reading variables from input.txt file.
	while (!textFile.eof())
	{
		textFile >> variables[counter];
		counter++;
	}

	if (counter > number)
	{
		cout << "ERROR! Integers are more than size." << endl;
		system("pause");
		return 0;
	}

	if (counter < number)
	{
		cout << "ERROR! Integers are less than size." << endl;
		system("pause");
		return 0;
	}

	//! Printing the values ​​returned from functions.
	cout << "Sum is " << SumOperator(variables, number) << endl;
	cout << "Product is " << productOperator(variables, number) << endl;
	cout << "Average is " << averageOperator(variables, number) << endl;
	cout << "Smallest is " << smallestOperator(variables, number) << endl;

	cout << endl;
	system("pause");
}

/**
* @brief Define sum function.
* @param sum is an integer variable that sums variables.
* @return return the sum of the variables.
*/
int SumOperator(int *A, int num)
{
	int sum = 0;

	for (int i = 0; i < num; i++)
	{
		sum = sum + A[i];
	}
	return sum;
}

/**
* @brief Define product function.
* @param product is an integer variable that products variables.
* @return return the product of the variables.
*/
int productOperator(int *A, int num)
{
	int product = 1;

	for (int i = 0; i < num; i++)
	{
		product = product * A[i];
	}
	return product;
}

/**
* @brief Define average function.
* @param average is an float variable that averages variables.
* @param sum is an integer variable that sums variables.
* @return return the average of the variables.
*/
float averageOperator(int *A, int num)
{
	float average = 0;
	int sum = 0;

	sum = SumOperator(A, num);

	average = (float)sum / num;
	return average;
}

/**
* @brief Define the function finding the smallest element.
* @param min is an integer variable that finding the minimum variable.
* @return return the minimum element of the variables.
*/
int smallestOperator(int *A, int num)
{
	int min = 0;

	min = A[0];
	for (int i = 0; i < num; i++)
	{
		if (A[i] < min)
		{
			min = A[i];
		}
	}
	return min;
}