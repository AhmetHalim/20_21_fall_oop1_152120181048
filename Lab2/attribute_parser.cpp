#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <string>
using namespace std;

int main() {
    int num, que;
    cin >> num >> que;
    
    unordered_map<string, unordered_map<string,string>> tag_map;
    unordered_map<string, string> tag_ownership;
    std::vector<string> vec;
    
    for(auto i=0; i != num+1; i++)
    {
        string input;
        std::getline(cin,input);
        
        if(i != 0)
        {
            if(input.find("</")!=string::npos)
            {
                vec.pop_back();
            }
            else
            {
                auto pos = input.find(" ");
                auto tag_name = input.substr(1,pos-1);
                if(tag_name.back() == '>')
                {
                    tag_name = tag_name.substr(0,tag_name.length()-1);
                }
                if(vec.empty())
                {
                    tag_ownership.insert(pair<string, string>(tag_name,""));
                }
                else
                {
                    tag_ownership.insert(pair<string,string>(tag_name,vec.back()));
                }
                vec.push_back(tag_name);
                unordered_map<string,string> attr_map;
                input.erase(0,pos+1);
                while(input.find(" = ") != string::npos)
                {
                    pos = input.find(" = ");
                    auto attr_name = input.substr(0,pos);
                    input.erase(0,pos+4);
                    auto q_pos = input.find("\"");
                    auto attr_value = input.substr(0,q_pos);
                    attr_map.insert(pair<string,string>(attr_name,attr_value));
                    input.erase(0,q_pos+2);
                }
                tag_map.insert(pair<string,unordered_map<string,string>>(tag_name,attr_map));
            }
        }
    }
    for(auto i=0; i!=que; i++)
    {
        bool valid = true;
        string input;
        cin >> input;
        auto dot_pos = input.find('.');
        string outer;
        
        if(dot_pos!=string::npos)
        {
            while(dot_pos!=string::npos && valid)
            {
                valid = false;
                auto outer = input.substr(0,dot_pos);
                input.erase(0,dot_pos+1);
                dot_pos = input.find('.');
                
                if(dot_pos == string::npos)
                {
                    auto t_pos = input.find('~');
                    auto inner = input.substr(0,t_pos);
                    (tag_ownership[inner] == outer)? valid = true: valid = false;
                }
                else
                {
                    auto middle = input.substr(0,dot_pos);
                    (tag_ownership[middle] == outer)? valid = true: valid = false;
                }
            }
        }
        else
        {
            auto pos = input.find('~');
            auto tag = input.substr(0,pos);
            
            if(not tag_ownership[tag].empty())
            {
                valid = false;
            }
        }
        if(not valid)
        {
            cout << "Not Found!\n";
        }
        else
        {
            auto pos = input.find('~');
            auto attr = input.substr(pos+1,input.length()-pos-1);
            auto tag = input.substr(0,pos);
            
            if(tag_map[tag][attr].empty())
            {
                cout << "Not Found!\n";
            }
            else
            {
                cout << tag_map[tag][attr] << endl;
            }
        }
    }
    return 0;
}
